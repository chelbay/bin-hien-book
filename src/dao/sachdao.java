package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import bean.sachbean;

public class sachdao {
	public ArrayList<sachbean> getsach() throws Exception {
		ArrayList<sachbean> ds = new ArrayList<sachbean>();
		// b1: kết nối vào csdl
		DungChung dc = new DungChung();
		dc.KetNoi();
		// b2: thiết lập câu lệnh sql
		String sql = "select*from sach";
		// b3: coho thực hiện câu lệnh sql
		PreparedStatement cmd = dc.cn.prepareStatement(sql);
		ResultSet rs = cmd.executeQuery();
		// b4: duyet qua cac loai va luu vao ds
		while (rs.next()) {
			String masach = rs.getString("masach");
			String tensach = rs.getString("tensach");
			int soluong = rs.getInt("soluong");
			long gia = rs.getLong("gia");
			String maloai = rs.getString("maloai");
			String sotap = rs.getString("sotap");
			String anh = rs.getString("anh");
			Date ngaynhap = rs.getDate("NgayNhap");
			String tacgia = rs.getString("tacgia");

			sachbean sach = new sachbean(masach, tensach, soluong, gia, maloai, sotap, anh, ngaynhap, tacgia);
			ds.add(sach);
		}
		rs.close();
		dc.cn.close();
		return ds;
	}

	public int themSach(String masach, String tensach, int soluong, long gia, String maloai, String sotap,
			String anh, String tacgia, Date ngaynhap) throws Exception {
		
		if(ngaynhap == null) {
			ngaynhap = new Date();
		}
		
		DungChung dc = new DungChung();
		dc.KetNoi();
		String sql = "INSERT INTO sach VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stmt = dc.cn.prepareStatement(sql);
		stmt.setString(1, masach);
		stmt.setString(2, tensach);
		stmt.setInt(3, soluong);
		stmt.setLong(4, gia);
		stmt.setString(5, maloai);
		stmt.setString(6, sotap);
		stmt.setString(7, anh);
		stmt.setTimestamp(8, new Timestamp( ngaynhap.getTime()  ));
		stmt.setString(9, tacgia);
		
		System.out.println(stmt.toString());
		
		int cnt = stmt.executeUpdate();
		dc.cn.close();
		return cnt; // return id
	}
	
	public int capNhatSach(String masach, String tensach, int soluong, long gia, String maloai, String sotap,
			String anh, String tacgia, Date ngaynhap) throws Exception {
		
		if(ngaynhap == null) {
			ngaynhap = new Date();
		}
		
		DungChung dc = new DungChung();
		dc.KetNoi();
		String sql = "UPDATE sach SET "  
					+ " tensach=? "
					+ " , soluong=? "
					+ " , gia=? "
					+ " , maloai=? "
					+ " , sotap=? ";
		if(anh  != null) {
			sql = sql + " , anh=? ";
		}
		sql = sql	+ " , tacgia=? "
					+ " , NgayNhap=? "
					+ "  WHERE masach=? ";
		PreparedStatement stmt = dc.cn.prepareStatement(sql);
		int i = 0;
		stmt.setString(++i, tensach);
		stmt.setInt(++i, soluong);
		stmt.setLong(++i, gia);
		stmt.setString(++i, maloai);
		stmt.setString(++i, sotap);
		if(anh  != null) {
			stmt.setString(++i, anh);
		}
		stmt.setString(++i, tacgia);
		stmt.setTimestamp(++i, new Timestamp( ngaynhap.getTime()  ));
		stmt.setString(++i, masach);
		
		System.out.println(stmt.toString());
		int cnt = stmt.executeUpdate();
		dc.cn.close();
		return cnt; // return id
		
		
	}
	
	public int xoaSach(String masach) throws Exception{
		DungChung db = new DungChung();
		db.KetNoi();
		String sql = "DELETE FROM sach WHERE masach = ? ";
		PreparedStatement stmt = db.cn.prepareStatement(sql);
		stmt.setString(1, masach);
		int cnt = stmt.executeUpdate();
		System.out.println("cnt = " + cnt);
		return cnt;
		
		
		
	}

}
