package controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import java.util.List;

import bean.sachbean;

import bo.sachbo;

/**
 * Servlet implementation class AREcontroller
 */
@WebServlet("/AREcontroller")
public class AREcontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AREcontroller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	request.setCharacterEncoding("utf-8");
    	
    	
		DiskFileItemFactory factory = new DiskFileItemFactory();
		DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
		String dirUrl1 = request.getServletContext().getRealPath("") + File.separator + "files";
		response.getWriter().println(dirUrl1);
		String masach = "";
		String tensach = "";
		int soluong = 0;
		long gia=0;
		String maloai = "";
		String sotap = "";
		String tg = "";
		String savePath = "image_sach/";
		String imgName="";
		Date ngaynhap = null;
		String fileImg = null;
		
		
		try {
			sachbo sbo=new sachbo();
			if(request.getParameter("maXoa") != null) {
				try {
					int res = sbo.xoaSach(request.getParameter("maXoa"));
						request.setAttribute("checkXoa", "Xóa thành công");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			List<FileItem> fileItems = upload.parseRequest(request);
			for (FileItem fileItem : fileItems) {
				if (!fileItem.isFormField()) {
					imgName = fileItem.getName();
					savePath = savePath.concat(imgName);
					if (!imgName.equals("")) {
						String dirUrl = request.getServletContext().getRealPath("") + File.separator + "files";
						int vt = dirUrl.indexOf(".metadata");
						dirUrl = dirUrl.substring(0, vt);
						dirUrl = dirUrl +  "Book\\WebContent\\image_sach";
						File dir = new File(dirUrl);
						if (!dir.exists()) {
							dir.mkdir();
						}
						fileImg = dirUrl + File.separator + imgName;
						File file = new File(fileImg);
						try {
							fileItem.write(file);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else{// Neu la control
					String tentk = fileItem.getFieldName();

					if (tentk.equals("txtms"))
						masach = fileItem.getString("utf-8");
					if (tentk.equals("txtts")) {
						tensach = fileItem.getString("utf-8");
					}
					if (tentk.equals("txtml"))
						maloai = fileItem.getString("utf-8");
					if(tentk.equals("txtsl"))
						soluong = Integer.parseInt(fileItem.getString());
					if(tentk.equals("txtdg"))
						gia = Long.parseLong(fileItem.getString());
					if(tentk.equals("txtst"))
						sotap = fileItem.getString();
					if(tentk.equals("txttg"))
						tg = fileItem.getString("utf-8");
					if(tentk.equals("txtdate")) {
						String ngaynhap2 = fileItem.getString("utf-8");
						DateFormat f= new SimpleDateFormat("yyyy-MM-dd");
						ngaynhap = f.parse(ngaynhap2);
					}
					System.out.println("USER ID" + masach + "tensach" + tensach +"maloai"+maloai +"so luong"+soluong+"gia"+gia+"so tap"+sotap+"tacgia"+tg+ "tenfile" + savePath);
				}

			}
			if(request.getParameter("action").equals("INSERT")) {
				sbo.themSach(masach, tensach, soluong,gia,maloai,sotap,savePath,tg, ngaynhap); 
			}else {
				sbo.capNhatSach(masach, tensach, soluong,gia,maloai,sotap, (imgName.equals("") ? null : savePath) ,tg, ngaynhap); 
			}
			response.sendRedirect("/Book/htsachcontroller");
			return;
		} catch (FileUploadException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
