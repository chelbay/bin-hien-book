<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.sachbean"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">  -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<title>Insert title here</title>
<style>
table, th, td {
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	padding: 3px;
}

#t01 tr:nth-child(even) {
	background-color: #eee;
}

#t01 tr:nth-child(odd) {
	background-color: #fff;
}

table {
	width: 100%;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

	<form action="AREcontroller?action=INSERT" method="POST"
		enctype="multipart/form-data" id="formBook">
		<input type="hidden" name ="action" value="INSERT">
	

		Mã sách: <input type="text" placeholder="Nhập mã sách" name="txtms"
			required> <br> Tên sách: <input type="text"
			placeholder="Nhập tên sách" name="txtts" required> <br>
		Số lượng: <input type="text" placeholder="Nhập số lượng" name="txtsl"
			required> <br> Đơn giá: <input type="text"
			placeholder="Nhập đơn giá" name="txtdg" required> <br>
		Mã loại: <input type="text" placeholder="Nhập mã loại" name="txtml"
			required> <br> Số tập: <input type="text"
			placeholder="Nhập số tập" name="txtst" required> <br>
		Ảnh: <input type="file" name="txtfile"> <br> Ngày nhập: <input
			type="date" placeholder="" name="txtdate" required> <br>
		Tác giả: <input type="text" placeholder="Nhập tác giả" name="txttg"
			required> <br>
		<button id="btnCreate" type="submit" name="btnCreate"
			class="btn btn-success">Thêm mới</button>
	</form>
	<br>

	<%
		ArrayList<sachbean> ds = (ArrayList<sachbean>) request.getAttribute("dssach");
	
	%>

	<table id="t01">
		<tr>
			<th>Mã sách</th>

			<th>Tên sách</th>

			<th>Số lương</th>

			<th>Giá</th>

			<th>Mã loại</th>

			<th>Số tập</th>

			<th>Ảnh</th>

			<th>Ngày nhập</th>

			<th>Tác giả</th>
			<th></th>

		</tr>

		<%
			DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			
			for (sachbean l : ds) {
		%>

		<tr>
			<td><%=l.getMasach()%></td>

			<td>
			<%=l.getTensach()%>
			
			
			
			
			
			</td>

			<td><%=l.getSoluong()%></td>

			<td><%=l.getGia()%></td>

			<td><%=l.getMaloai()%></td>

			<td><%=l.getSotap()%></td>

			<td><img src="<%=l.getAnh()%>" width="50" height="70"></td>

			<td><%=l.getNgaynhap()%></td>

			<td><%=l.getTacgia()%></td>
			<td>
				<button class="btn-update"
					data-masach="<%= l.getMasach() %>"
					data-tensach="<%= l.getTensach() %>"
					data-soluong="<%= l.getSoluong() %>"	
					data-gia="<%= l.getGia() %>"
					data-maloai="<%= l.getMaloai() %>"
					data-sotap="<%= l.getSotap() %>"
					data-anh="<%= l.getAnh() %>"
					data-ngaynhap="<%= f.format(l.getNgaynhap()) %>"
					data-tacgia="<%= l.getTacgia() %>"
				
				>Cập nhật</button>
				<button>
				<a href="AREcontroller?maXoa=<%= l.getMasach() %>" class="btn btn-danger" style="text-decoration: none;">Xóa</a>
				</button>
			</td>
			
		</tr>
		<%
			}
		%>
	</table>
	
	
	
	
	
	 

	<script>
		$(".btn-update").click(function(){
			//$("#formBook input[name='action']").val("UPDATE");
			$("#formBook").attr("action", "AREcontroller?action=UPDATE");
			$("#formBook input[name='txtms']").attr("readonly", "readonly");
			$("#formBook input[name='txtms']").val($(this).data("masach"));
			$("#formBook input[name='txtts']").val($(this).data("tensach"));
			$("#formBook input[name='txtsl']").val($(this).data("soluong"));
			$("#formBook input[name='txtdg']").val($(this).data("gia"));
			$("#formBook input[name='txtml']").val($(this).data("maloai"));
			$("#formBook input[name='txtst']").val($(this).data("sotap"));
			$("#formBook input[name='txttg']").val($(this).data("tacgia"));
			$("#formBook input[name='txtdate']").val($(this).data("ngaynhap"));
			$("#formBook input[name='txtfile']").removeAttr("required");
			$("#btnCreate").html("Cập nhật");
		});
		<% if(request.getAttribute("checkXoa") != null) { %>
		alert("<%= request.getAttribute("checkXoa") %>");
		<% } %>
	</script>
	
</body>
</html>